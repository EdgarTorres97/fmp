import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import{AuthService}from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  public estado: boolean;
  constructor(
    private authService: AuthService,
    private router: Router
  ){
    this.estado = authService.obtenerToken();
  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if(this.estado){
        return true;    
      }else{
        this.router.navigate(["/acceder"]);
        // this.router.navigateByUrl("acceder");
      }
      
  }
  
}
