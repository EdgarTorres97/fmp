import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import{AuthService}from '../services/auth.service';
import{User} from '../models/user';
@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {
  public user: User;
  public estado: boolean;
  constructor(
    private authService: AuthService,
    private router: Router
  ){
    this.user = authService.retornarDataUser();
    this.estado = authService.obtenerToken();
  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if(this.estado == true){
        if(this.user.id_role != 1&&this.user.id_role != 2){
          this.router.navigate(["error"]);
        }else{
          return true;
        }
      }else{
        this.router.navigate(["acceder"]);
      }
  }
  
}
