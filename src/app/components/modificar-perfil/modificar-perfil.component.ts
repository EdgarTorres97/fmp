import { Component, OnInit } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import{Router} from '@angular/router';
import{AuthService}from '../../services/auth.service';
import { User } from 'src/app/models/user';
import swal from'sweetalert2';
@Component({
  selector: 'app-modificar-perfil',
  templateUrl: './modificar-perfil.component.html',
  styleUrls: ['./modificar-perfil.component.css']
})
export class ModificarPerfilComponent implements OnInit {
  public token: string;
  public titulo: string;
  public usuario: User;
  public swal: any;
  public api: string;
  httpOptions = {
    headers: new HttpHeaders()
  };
  constructor(
    public httpVar:HttpClient,
    private router: Router,
    private authService: AuthService,
  ) {
    this.usuario = authService.retornarDataUser();
    this.titulo = 'Modificar perfil de '+this.usuario.name;
    this.token = authService.retornarToken();
    this.httpOptions.headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    this.api = this.authService.api;
   }

  ngOnInit(): void {
    // console.log(this.usuario);
  }

  onSubmit(){
    // console.log(this.usuario);
    //console.log(this.cuenta);
    this.httpVar.post<User>(this.api+'modificarPerfil',this.usuario,this.httpOptions)
    .subscribe(data=>{
      // console.log(data);
      
      if(data['exito']){

        localStorage.setItem('currentUser', JSON.stringify({ token: this.usuario.name}));
        localStorage.setItem('currentPhoneNumber', JSON.stringify({ token: this.usuario.phoneNumber}));
        localStorage.setItem('currentAge', JSON.stringify({ token: this.usuario.age}));
        swal.fire({
          icon: 'success',
          title: 'Exito',
          text: 'Se ha actualizado correctamente',
        });
      }else{
        swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'A ocurrido un error',
        });
      }
    },error=>{

    })
  }
}
