import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigurarReporteComponent } from './configurar-reporte.component';

describe('ConfigurarReporteComponent', () => {
  let component: ConfigurarReporteComponent;
  let fixture: ComponentFixture<ConfigurarReporteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfigurarReporteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigurarReporteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
