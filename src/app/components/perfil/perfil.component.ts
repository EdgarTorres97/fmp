import { Component, OnInit } from '@angular/core';
import{User}from '../../models/user';
import{AuthService}from '../../services/auth.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {
  public usuario: User;
  constructor(
    private authService: AuthService,
  ) {
    this.usuario = authService.retornarDataUser();
   }

  ngOnInit(): void {
  }

}
