import { Component, OnInit } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import{Router} from '@angular/router';
import{AuthService}from '../../services/auth.service';
import { User } from '../../models/user';
import { NgxSpinnerService } from "ngx-spinner";
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  public titulo: string;
  public usuarios: User[];
  public rol: string;
  public token: string;
  public crearUsuario: boolean;
  public api: string;
  httpOptions = {
    headers: new HttpHeaders()
  };
  dtOptions: DataTables.Settings = {};
  constructor(
    public httpVar:HttpClient,
    private router: Router,
    private authService: AuthService,
    private spinner:NgxSpinnerService
  ) {
    
    this.token = authService.retornarToken();
    this.httpOptions.headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    this.titulo = "Usuarios FindMyPet";
    this.usuarios = [];
    this.rol = 'Administradores';
    this.crearUsuario = false;
    this.api = this.authService.api;
  }

  ngOnInit(): void {
    //this.obtenerListaUsuarios('usuarios');
    this.dtOptions = {
      pagingType: 'full_numbers'
    };
  }
  crearUsuarioDiv(){
    this.crearUsuario = !this.crearUsuario;
  }
  obtenerListaUsuarios(rol: string){

    if((rol === 'administradores')||(rol == 'colaboradores')||(rol === 'usuarios')){
      this.usuarios = [];
      this.rol = rol;
      // console.log('administradores');
      this.spinner.show();
      this.httpVar.get<User>(this.api+rol,this.httpOptions)
      .subscribe(data=>{
        //console.log(data[1]);
        // console.log(data);
        if(data){
          for (let key in data) {
            let value = data[key]['idTiporeporte'];
            //console.log(value);
            let customObj = new User(
              data[key]['id'],
              data[key]['name'],
              data[key]['email'],

              null,
              data[key]['phoneNumber'],
              data[key]['age'],
              data[key]['id_role'],
              data[key]['urlImage'],
              null
              );
              this.usuarios.push(customObj);
            
          }
          this.spinner.hide();
          
        }else{
          this.spinner.hide();
        }

      });
    }else {
      console.log('No tienes acceso');
    }
    
  }

}
