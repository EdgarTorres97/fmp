import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IniciocardsComponent } from './iniciocards.component';

describe('IniciocardsComponent', () => {
  let component: IniciocardsComponent;
  let fixture: ComponentFixture<IniciocardsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IniciocardsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IniciocardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
