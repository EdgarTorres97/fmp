import { Component, OnInit,Input } from '@angular/core';
import { NodeCompatibleEventEmitter } from 'rxjs/internal/observable/fromEvent';
import { LostPetReportViewModel } from 'src/app/models/lostPetReportViewModel';

@Component({
  selector: 'app-iniciocards',
  templateUrl: './iniciocards.component.html',
  styleUrls: ['./iniciocards.component.css']
})
export class IniciocardsComponent implements OnInit {


  perros = [
    {id: 1, name: {forename: 'Mascotas Perdidas', surname: 'perro1.jpg'}},
    {id: 2, name: {forename: 'Mascotas Encontradas', surname: 'perro2.jpg'}},

];
  constructor() { }

  ngOnInit(): void {
  }

}
