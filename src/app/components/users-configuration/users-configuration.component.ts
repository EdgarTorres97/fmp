import { Component, OnInit } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import{AuthService}from '../../services/auth.service';

import{Router,ActivatedRoute} from '@angular/router';
import { UserViewModel } from 'src/app/models/userViewModel';
import { Role } from 'src/app/models/role';
import swal from'sweetalert2';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-users-configuration',
  templateUrl: './users-configuration.component.html',
  styleUrls: ['./users-configuration.component.css']
})
export class UsersConfigurationComponent implements OnInit {
  public titulo: string;
  public user: UserViewModel;
  public token: string;
  public api: string;
  public roles: Role[];
  public role: string;
  public swal: any;
  httpOptions = {
    headers: new HttpHeaders()
  };
  constructor(
    public httpVar:HttpClient,
    private route: ActivatedRoute,
    
    private router: Router,
    private authService: AuthService,
    private spinner:NgxSpinnerService
  ) { 
    this.titulo = 'Configurar usuario';
    this.api = this.authService.api;
    this.token = authService.retornarToken();
    this.httpOptions.headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    this.role = '';
    this.roles = [];
    this.user = {
      id:Number(this.route.snapshot.paramMap.get('id')),
      name:null,
      email:null,
      phoneNumber:null,
      age: null,
      id_role: null,
      roleName:null,
      description:null,
      urlImage:null,
      password:null
    }
  }

  ngOnInit(): void {
    this.obtenerListaRoles();
    this.buscarUsuario();
  }
  buscarUsuario(){
    this.spinner.show();

    //  console.log(this.reporte);
      this.httpVar.post<UserViewModel>(this.api+'usuario',this.user,this.httpOptions)
      .subscribe(data=>{
       console.log(data);
        if(data[0]){
          
            this.user = {
            id: data[0]['id'],
            name:data[0]['name'],
            email:data[0]['email'],
            password:null,
            phoneNumber:data[0]['phoneNumber'],
            age: data[0]['age'],
            id_role: data[0]['id_role'],
            roleName:null,
            description:null,
            urlImage:data[0]['urlImage'],            
          }
          for (let role in this.roles) {
            if(data[0]['id_role']==this.roles[role]['id']){
              this.user.roleName = this.roles[role]['roleName'];
              console.log(this.user);
            }
          }
          this.spinner.hide();

          console.log(this.user);
       }else{
        this.spinner.hide();
       }
    });
  }
  obtenerListaRoles(){
    this.httpVar.get<Role>(this.api+'roles',this.httpOptions)
    .subscribe(data=>{
      //console.log(data[1]);
      console.log(data);
      for (let key in data) {
        //console.log(value);
        let customObj = 
        new Role(
          data[key]['id'],
          data[key]['roleName'],
          data[key]['description']
          );
        this.roles.push(customObj);
      }
    });
  }
  onSubmit(){
    console.log(this.user);
    //console.log(this.cuenta);
    
    this.httpVar.post<UserViewModel>(this.api+'modificarUsuario',this.user,this.httpOptions)
    .subscribe(data=>{
      console.log(data);
      
      if(data['exito']){
        swal.fire({
          icon: 'success',
          title: 'Exito',
          text: 'Se ha actualizado correctamente',
        });
        console.log(this.user.id_role);
        for (let role in this.roles) {
          if(this.roles[role]['id']==this.user.id_role){
            this.user.roleName = this.roles[role]['roleName'];
            console.log(this.user);
          }
        }
      }else{
        swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'A ocurrido un error',
        });
      }
    },error=>{

    });
    
  }
}
