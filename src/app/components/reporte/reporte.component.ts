import { Component, OnInit ,Input } from '@angular/core';
import { LostPetReportViewModel } from 'src/app/models/lostPetReportViewModel';

@Component({
  selector: 'app-reporte',
  templateUrl: './reporte.component.html',
  styleUrls: ['./reporte.component.css']
})
export class ReporteComponent implements OnInit {
  @Input() lostPetReport: LostPetReportViewModel;
  @Input() configurar: boolean;
  public estadoReporte: string;
  constructor() {
    this.estadoReporte = '';
   }

  ngOnInit(): void {
    if(this.lostPetReport.id_petProcess==1){
      this.estadoReporte = 'En revisión';
    }else if(this.lostPetReport.id_petProcess==2){
      this.estadoReporte = 'Perdido';
    }else if(this.lostPetReport.id_petProcess == 3){
      this.estadoReporte = 'Encontrado';
    }
  }

  
}
