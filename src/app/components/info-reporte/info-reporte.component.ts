import { Component, OnInit } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import{AuthService}from '../../services/auth.service';

import{Router,ActivatedRoute} from '@angular/router';
import { LostPetReportViewModel } from 'src/app/models/lostPetReportViewModel';
import { LostPetReport } from 'src/app/models/lostPetReport';
import { PetProcess } from 'src/app/models/PetProcess';
import swal from'sweetalert2';
@Component({
  selector: 'app-info-reporte',
  templateUrl: './info-reporte.component.html',
  styleUrls: ['./info-reporte.component.css']
})
export class InfoReporteComponent implements OnInit {
  public lostPetReport: LostPetReportViewModel;

  public token: string;
  public api: string;
  public swal: any;
  public titulo: string;
 httpOptions = {
   headers: new HttpHeaders()
 };
 public petProcesses: PetProcess[];
 constructor(
   public httpVar:HttpClient,
   private route: ActivatedRoute,
   
   private router: Router,
   private authService: AuthService,
 ) {
    this.titulo = 'Información de reporte' 
  this.api = this.authService.api;
   this.petProcesses = [];
   this.inicializarLostPetReport();
   this.token = authService.retornarToken();
   this.httpOptions.headers = new HttpHeaders({
     'Authorization': `Bearer ${this.token}`
   });
   
  }

 ngOnInit(): void {

   this.buscarReporte();
   
 }
 buscarReporte(){
   //  console.log(this.reporte);
     this.httpVar.post<LostPetReportViewModel>(this.api+'lostPetReportId',this.lostPetReport,this.httpOptions)
     .subscribe(data=>{
      console.log(data);
      if(data[0]){
       //console.log("entro");
       this.lostPetReport = {   
         id:data[0]['id'],
         id_user:data[0]['id_user'],
         id_petType:data[0]['id_petType'],
         id_petSex :data[0]['id_petSex'],
         id_petChip :data[0]['id_petChip'],
         id_petTag  :data[0]['id_petTag'],
         id_petProcess: data[0]['id_petProcess'],
         ppName:data[0]['ppName'],
         id_petLocation  :data[0]['id_petLocation'],
         lprBreedName  :data[0]['lprBreedName'],
         lprSize :data[0]['lprSize'],
         lprAge:data[0]['lprAge'],
         lprTail:data[0]['lprTail'],
         lprEar:data[0]['lprEar'],
         lprColor:data[0]['lprColor'],
         lprObservation:data[0]['lprObservation'],
         lprPhoneNumber:data[0]['lprPhoneNumber'],
         lprPetName:data[0]['lprPetName'],
         plLostDate:data[0]['plLostDate'],
         plLote:data[0]['plLote'],
         plManzana:data[0]['plManzana'],
         plPostalCode:data[0]['plPostalCode'],
         plRegion:data[0]['plRegion'],
         pllatitude:data[0]['pllatitude'],
         pllongitude:data[0]['pllongitude'],
         mnName:data[0]['mnName'],
         pcName:data[0]['pcName'],
         piImageUrl:data[0]['piImageUrl'],
         psName:data[0]['psName'],
         ptName:data[0]['ptName'],
         ptpName:data[0]['ptpName'],
       }
       console.log(this.lostPetReport);

     }
   });
 }

 inicializarLostPetReport(){
   this.lostPetReport = {
     id:Number(this.route.snapshot.paramMap.get('id')),
     id_user:null,
     id_petType: null,
     id_petSex: null,
     id_petChip: null,
     id_petTag: null,
     id_petProcess:null,
     ppName:null,
     id_petLocation: null,
     lprBreedName: null,
     lprSize: null,
     lprAge: null,
     lprPetName:null,
     lprPhoneNumber:null,
     lprTail: null,
     lprEar: null,
     lprColor: null,
     lprObservation: null,
     plLostDate: null,
     plLote: null,
     plManzana: null,
     plPostalCode: null,
     plRegion: null,
     pllatitude: null,
     pllongitude: null,
     mnName:null,
     pcName: null,
     piImageUrl: null,
     psName: null,
     ptName: null,
     ptpName: null,
     }
 }
}
