import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoReporteComponent } from './info-reporte.component';

describe('InfoReporteComponent', () => {
  let component: InfoReporteComponent;
  let fixture: ComponentFixture<InfoReporteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InfoReporteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoReporteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
