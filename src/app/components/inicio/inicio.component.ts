import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    $(document).ready(function() {
      var $magic = $(".magic"),
          magicWHalf = $magic.width() / 2;
      $(document).on("mousemove", function(e) {
        $magic.css({"left": e.pageX - magicWHalf, "top": e.pageY - magicWHalf});
      });
    });
  }

}
