import { Component, OnInit } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import{AuthService}from '../../services/auth.service';

import{Router,ActivatedRoute} from '@angular/router';
import { User } from 'src/app/models/user';
import { Role } from 'src/app/models/role';

@Component({
  selector: 'app-users-detail',
  templateUrl: './users-detail.component.html',
  styleUrls: ['./users-detail.component.css']
})
export class UsersDetailComponent implements OnInit {
  public titulo: string;
  public user: User;
  public token: string;
  public api: string;
  public roles: Role[];
  public role: string;
  httpOptions = {
    headers: new HttpHeaders()
  };
  constructor(
    public httpVar:HttpClient,
    private route: ActivatedRoute,
    
    private router: Router,
    private authService: AuthService,
  ) {
    this.titulo = 'Información del usuario';
    this.api = this.authService.api;
    this.token = authService.retornarToken();
    this.httpOptions.headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    this.role = '';
    this.roles = [];
    this.user = {
      id:Number(this.route.snapshot.paramMap.get('id')),
      name:null,
      email:null,
      phoneNumber:null,
      age: null,
      id_role: null,
      urlImage:null,
      password:null,
      token:null
    }
   }

  ngOnInit(): void {
    this.obtenerListaRoles();
    this.buscarReporte();
  }
  buscarReporte(){
    //  console.log(this.reporte);
      this.httpVar.post<User>(this.api+'usuario',this.user,this.httpOptions)
      .subscribe(data=>{
       console.log(data);
        if(data[0]){
          for (let role in this.roles) {
            if(data[0]['id_role']==this.roles[role]['id']){
              this.role = this.roles[role]['roleName'];
            console.log(this.role);
            }
          }
            this.user = {
            id: data[0]['id'],
            name:data[0]['name'],
            email:data[0]['email'],
            password:null,
            phoneNumber:data[0]['phoneNumber'],
            age: data[0]['age'],
            id_role: data[0]['id_role'],
            urlImage:data[0]['urlImage'],
            token:null
          }
          console.log(this.user);
       }
    });
  }
  obtenerListaRoles(){
    this.httpVar.get<Role>(this.api+'roles',this.httpOptions)
    .subscribe(data=>{
      //console.log(data[1]);
      console.log(data);
      for (let key in data) {
        //console.log(value);
        let customObj = 
        new Role(
          data[key]['id'],
          data[key]['roleName'],
          data[key]['description']
          );
        this.roles.push(customObj);
      }
    });
  }
}
