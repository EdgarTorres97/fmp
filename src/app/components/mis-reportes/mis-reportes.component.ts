import { Component, OnInit } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import{Router,ActivatedRoute} from '@angular/router';
import{AuthService}from '../../services/auth.service';
import { LostPetReport } from 'src/app/models/lostPetReport';
import { LostPetReportViewModel } from 'src/app/models/lostPetReportViewModel';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-mis-reportes',
  templateUrl: './mis-reportes.component.html',
  styleUrls: ['./mis-reportes.component.css']
})
export class MisReportesComponent implements OnInit {
  httpOptions = {
    headers: new HttpHeaders()
  };
  public token: string;
  public lostPetReports: LostPetReportViewModel[];
  public error: string;
  public errorBol: boolean;
  public api: string;
  constructor(
    public httpVar:HttpClient,
    private router: Router,
    private authService: AuthService,
    private spinner:NgxSpinnerService
  ) {
    this.token = authService.retornarToken();
    this.httpOptions.headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    this.lostPetReports = [];
    this.error = '';
    this.errorBol = false;
    this.api = this.authService.api;
   }

  ngOnInit(): void {
    this.obtenerListaReportes();
  }
  obtenerListaReportes(){ 
    this.spinner.show();

    this.httpVar.get<LostPetReport>(this.api+'misReportes',this.httpOptions)
    .subscribe(data=>{
      // console.log(data);

      if(data){
        if(!data['error']){
          this.errorBol = false;
          for (let key in data) {
            let value = data[key]['id'];
            //console.log(value);
            let customObj = new LostPetReportViewModel(
              data[key]['id'],
              data[key]['id_user'],
              data[key]['id_petType'],
              data[key]['id_petSex'],
              data[key]['id_petChip'],
              data[key]['id_petTag'],
              data[key]['id_petProcess'],
              data[key]['ppName'],
              data[key]['id_petLocation'],
              data[key]['lprBreedName'],
              data[key]['lprSize'],
              data[key]['lprAge'],
              data[key]['lprTail'],
              data[key]['lprEar'],
              data[key]['lprColor'],
              data[key]['lprObservation'],
              data[key]['lprPhoneNumber'],
              data[key]['lprPetName'],
              data[key]['plLostDate'],
              data[key]['plLote'],
              data[key]['plManzana'],
              data[key]['plPostalCode'],
              data[key]['plRegion'],
              data[key]['pllatitude'],
              data[key]['pllongitude'],
              data[key]['mnName'],
              data[key]['pcName'],
              data[key]['piImageUrl'],
              data[key]['psName'],
              data[key]['ptName'],
              data[key]['ptpName'],
            );
            
            this.lostPetReports.push(customObj);
            
          }
          this.spinner.hide();
        }else{
          this.spinner.hide();
          this.errorBol = true;
          this.error = data['error'];
          
        }
      }
      else{
        this.spinner.hide();
        this.errorBol = true;
        this.error = 'A ocurrido un error';
      }
    })
  }
}
