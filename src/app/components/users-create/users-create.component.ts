import { Component, OnInit } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import{Router} from '@angular/router';
import{AuthService}from '../../services/auth.service';
import{User}from '../../models/user';
import { Role } from 'src/app/models/role';
@Component({
  selector: 'app-users-create',
  templateUrl: './users-create.component.html',
  styleUrls: ['./users-create.component.css']
})
export class UsersCreateComponent implements OnInit {
  public titulo: string;
  public user: User;
  public token: string;
  public roles: Array<object>;
  public labelNombre: string;
  public smallNombre: string;
  public labelEmail: string;
  public smallEmail: string;
  public labelRol: string;
  public smallRol: string;
  public labelPassword: string;
  public smallPassword: string;
  public mensaje: string;
  public mensajeError: string;
  public estatusCreado: boolean;
  public error: boolean;
  httpOptions = {
    headers: new HttpHeaders()
  };
  public api: string;
  constructor(
    public httpVar:HttpClient,
    private router: Router,
    private authService: AuthService,
    
  ) { 
    this.token = authService.retornarToken();
      this.httpOptions.headers = new HttpHeaders({
        'Authorization': `Bearer ${this.token}`
      }); 
      this.api = this.authService.api;
      this.titulo = "Crear user Conagua";
      this.labelNombre = "Nombre del user";
      this.labelEmail = "Correo del user";
      this.labelRol = "Selecciona el Rol";
      this.labelPassword ="Ingrese la contraseña";
      this.mensaje = "Exito";
      this.mensajeError = "A ocurrido un erro";
      this.error = false;
      this.estatusCreado = false;
      this.user = {
        id:null,
        name:null,
        email:null,
        phoneNumber:null,
        age: null,
        id_role: null,
        urlImage:null,
        password:null,
        token: null
      }
      this.roles = [];
  }

  ngOnInit(): void {
    this.obtenerListaRoles();
  }
  obtenerListaRoles(){
    this.httpVar.get<Role>(this.api+'roles',this.httpOptions)
    .subscribe(data=>{
      //console.log(data[1]);
      console.log(data);
      for (let key in data) {
        //console.log(value);
        let customObj = 
        new Role(
          data[key]['id'],
          data[key]['roleName'],
          data[key]['description']
          );
        this.roles.push(customObj);
      }
    });
  }
  onSubmit(){
    if(this.user.name=="" || this.user.name==null){
      this.smallNombre = "Debe ingresar un titulo";
    }
    console.log(this.user);
    this.httpVar.post<User>(this.api+'agregarUsuario',this.user,this.httpOptions)
    .subscribe(data=>{
      console.log(data);
      if(data){
        if(data['mensaje']){
          this.estatusCreado = true;
          this.error = false;
          this.mensaje = data['mensaje'];
          window.location.reload();
          
        }else{
          this.error = true;
          this.mensajeError = data['error'];
          this.estatusCreado = false;
        }
      }else{
        this.error = true;
      }
      
      //console.log(data);
    })

  }
}
