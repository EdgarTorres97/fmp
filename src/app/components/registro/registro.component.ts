import { Component, OnInit } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import{User}from '../../models/user';
import { NgxSpinnerService } from "ngx-spinner";
import{AuthService}from '../../services/auth.service';
@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {
  public usuario: User;
  public err:boolean;
  public api: string;
  public userEncrypt: User;
  public encPass: string;

  constructor(
    public httpVar:HttpClient,
    private spinner:NgxSpinnerService,
    private authService: AuthService
  ) {
    this.err=false;
    this.usuario = {
      id: null,
      name: '',
      email: '',
      phoneNumber:'',
      id_role: 0,
      password: '',
      age: null,
      urlImage:'',
      token:'',
    };
    this.api = this.authService.api;
    this.userEncrypt = {
      id:null,
      name:null,
      email:null,
      phoneNumber:null,
      age: null,
      id_role: null,
      urlImage:null,
      password:null,
      token:null
    };
   }

  ngOnInit(): void {
  }

  onSubmit() {

    try {
    this.spinner.show();
    this.err=false;
    //console.log(this.usuario);
    this.httpVar.post<User>(this.api+'register',this.usuario)
    .subscribe(data=>{
      
      setTimeout(() => {
        console.log(data);
        this.err=false;
        const now = new Date()
        let value;
        let ttl;
        // `item` is an object which contains the original value
        // as well as the time when it's supposed to expire
        const item = {
          value: value,
          expiry: now.getTime(),
        }
        this.userEncrypt = {
          id: this.authService.encriptar(data['id'].toString()),
          name:this.authService.encriptar(data['name']),
          email:this.authService.encriptar(data['email']),
          phoneNumber:this.authService.encriptar(data['phoneNumber']),
          age: this.authService.encriptar(data['age'].toString()),
          id_role: this.authService.encriptar(data['id_role'].toString()),
          urlImage:this.authService.encriptar(data['urlImage']),
          password: '',
          token:this.authService.encriptar(data['token'])
        };
        // console.log(this.userEncrypt);
        localStorage.setItem('usuario', JSON.stringify(this.userEncrypt));
        // console.log(this.authService.desencriptar(this.userEncrypt.token));
        /** spinner ends after 5 seconds */
        this.spinner.hide();
        window.location.reload();
      }, 500);


    },
    error=>{
      setTimeout(() => {
        this.spinner.hide();
        console.log("error");
        this.err=true;
      }, 500);
      
    });
    
    } catch (error) {
      setTimeout(() => {
        this.spinner.hide();
      }, 1000);
     

    }
  }
}
