import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import{Router} from '@angular/router';
import{User}from '../../models/user';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  public estado: boolean;
  
  public homeText = 'Prueba input';
  public user: User;
  constructor(
    authService: AuthService,
    private router: Router
  ) { 
    this.estado = authService.obtenerToken();
    this.user = authService.retornarDataUser();
  }

  ngOnInit(): void {
    //console.log(this.user);
  }
  CerrarSesion(){
    localStorage.removeItem('currentToken');
    localStorage.clear();
    this.estado = false;
    //this.router.navigateByUrl("login");
    window.location.reload();
    
  }

}
