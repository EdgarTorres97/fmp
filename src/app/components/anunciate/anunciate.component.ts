import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { Municipality } from 'src/app/models/municipality';
import { PetType } from 'src/app/models/petType';
import { PetChip } from 'src/app/models/petChip';
import { PetTag } from 'src/app/models/petTag';
import { PetSex } from 'src/app/models/petSex';
import { LostPetReport } from 'src/app/models/lostPetReport';
import swal from'sweetalert2';
import { NgForm } from '@angular/forms';
import * as Mapboxgl from 'mapbox-gl';
import { environment } from './../../../environments/environment';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-anunciate',
  templateUrl: './anunciate.component.html',
  styleUrls: ['./anunciate.component.css']
})
export class AnunciateComponent implements OnInit {
  public titulo: string;
  public api: string;
  public token: string;
  public municipios: Municipality[];
  public petTypes: PetType[];
  public petChips: PetChip[];
  public petTags: PetTag[];
  public petSexes: PetSex[];
  public lostPetReport: LostPetReport;
  public swal: any;
  httpOptions = {
    headers: new HttpHeaders()
  };

  filedata:any;

  constructor(
    public httpVar:HttpClient,
    private router: Router,
    private authService: AuthService,
    private spinner:NgxSpinnerService
  ) {
    this.titulo = 'Reporte de mascota pertida';
    this.api = this.authService.api;
    this.token = authService.retornarToken();
    this.httpOptions.headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    this.municipios = [];
    this.petTypes = [];
    this.petChips = [];
    this.petTags = [];
    this.petSexes = [];
    this.inicializarLostPetReport();

    }

    fileEvent(e){
        this.filedata = e.target.files[0];
    }
    mapa: Mapboxgl.Map;
  ngOnInit(): void {
    (Mapboxgl as any).accessToken =environment.mapboxKey;
    this.mapa = new Mapboxgl.Map({
      container: 'mapa-mapbox',
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [-86.8896912, 21.1559494],
      zoom: 13.22
      });

      this.mapa.addControl(new Mapboxgl.NavigationControl());
    this.crearmarkador(-86.8896912, 21.1559494);
    this.obtenerListaMunicipios();
    this.obtenerListaPetTypes();
    this.obtenerListaPetChips();
    this.obtenerListaPetTags();
    this.obtenerListaPetSexs();
    this.lostPetReport.id_petType = 1
    this.lostPetReport.id_petTag = 1
    this.lostPetReport.id_petSex = 1;
    this.lostPetReport.petLocation.id_municipality = 1;
    this.lostPetReport.id_petChip = 1;
    //console.log(this.lostPetReport);
    }

    crearmarkador(lng:number,lat:number){

      const marker=new Mapboxgl.Marker({
        draggable:true}).setLngLat([lng,lat])
      .addTo(this.mapa);

       marker.on('drag',() =>{
         //console.log(marker.getLngLat());
         this.lostPetReport.petLocation.plLatitude = marker.getLngLat().lat;
         this.lostPetReport.petLocation.plLongitude = marker.getLngLat().lng;
       })

    }



  inicializarLostPetReport(){
    this.lostPetReport = {
      id:null,
      id_user:null,
      id_petType: null,
      petType: {
        id:null,
        ptpCounter:null,
        ptpName: null
      },
      id_petSex: null,
      petSex: {
        id:null,
        psCounter:null,
        psName: null
      },
      id_petChip: null,
      petChip: {
        id: null,
        pcCounter: null,
        pcName: null
      },
      id_petTag: null,
      petTag:{
        id: null,
        ptCounter: null,
        ptName: null
      },
      id_petProcess:null,
      id_petLocation: null,
      petLocation:{
        id:null,
        plLostDate: null,
        plLote: null,
        plManzana: null,
        plPostalCode: null,
        plRegion: null,
        plLatitude: null,
        plLongitude: null,
        id_municipality:null,
        plMunicipality: {
          id: null,
          mnCounter: null,
          mnName: null
        }

      },
      lprBreedName: null,
      lprSize: null,
      lprAge: null,
      lprPetName:null,
      lprPhoneNumber:null,
      lprTail: null,
      lprEar: null,
      lprColor: null,
      lprObservation: null,
      }
  }
  obtenerListaMunicipios(){
    this.httpVar.get<Municipality>(this.api+'municipios',this.httpOptions)
    .subscribe(data=>{
      //console.log(data[1]);
      //console.log(data);
      for (let key in data) {
        //console.log(value);
        let customObj =
        new Municipality(
          data[key]['id'],
          data[key]['mnName'],
          null
          );
        this.municipios.push(customObj);
      }
    });
  }
  obtenerListaPetTypes(){
    this.httpVar.get<PetType>(this.api+'petTypes',this.httpOptions)
    .subscribe(data=>{
      //console.log(data[1]);
      //console.log(data);
      for (let key in data) {
        //console.log(value);
        let customObj =
        new PetType(
          data[key]['id'],
          data[key]['ptpName'],
          null
          );
        this.petTypes.push(customObj);
      }
    });
  }
  obtenerListaPetChips(){
    this.httpVar.get<PetChip>(this.api+'petChips',this.httpOptions)
    .subscribe(data=>{
      //console.log(data[1]);
      //console.log(data);
      for (let key in data) {
        //console.log(value);
        let customObj =
        new PetChip(
          data[key]['id'],
          data[key]['pcName'],
          null
          );
        this.petChips.push(customObj);
      }
    });
  }
  obtenerListaPetTags(){
    this.httpVar.get<PetTag>(this.api+'petTags',this.httpOptions)
    .subscribe(data=>{
      //console.log(data[1]);
      //console.log(data);
      for (let key in data) {
        //console.log(value);
        let customObj =
        new PetTag(
          data[key]['id'],
          data[key]['ptName'],
          null
          );
        this.petTags.push(customObj);
      }
    });
  }
  obtenerListaPetSexs(){
    this.httpVar.get<PetSex>(this.api+'petSexes',this.httpOptions)
    .subscribe(data=>{
      //console.log(data[1]);
      //console.log(data);
      for (let key in data) {
        //console.log(value);
        let customObj =
        new PetSex(
          data[key]['id'],
          data[key]['psName'],
          null
          );
        this.petSexes.push(customObj);
      }
    });
  }
  subirImagen(id_lostPetReport) {

    var myFormData = new FormData();

    myFormData.append('image', this.filedata);
    myFormData.append('id_lostPetReport', id_lostPetReport);
    //console.log(myFormData);
    this.httpVar.post(this.api+'subirImagenReporte', myFormData, this.httpOptions)
    .subscribe(data => {
      //console.log(data);
      if(data){
        if(data['exito']){
          this.spinner.hide();
          swal.fire({
            icon: 'success',
            title: 'Exito',
            text: 'Se ha enviado correctamente',
          });
        }else{
          this.spinner.hide();
          swal.fire({
            icon: 'error',
            title: 'A ocurrido un error',
            text: 'Subiendo la imagen',
          });
        }

      }else{
        this.spinner.hide();
        swal.fire({
          icon: 'error',
          title: 'A ocurrido un error',
          text: 'Subiendo la imagen',
        });
      }
    });

  }
  onSubmit() {
    this.spinner.show();
    //console.log(this.lostPetReport);
    this.httpVar.post<LostPetReport>(this.api+'crearReporte',this.lostPetReport,this.httpOptions)
    .subscribe(data=>{
      //console.log(data);
      if(data){
        if(data['exito']){
          this.subirImagen(data['id_lostPetReport']);

          //this.inicializarLostPetReport();
        }else{
          swal.fire({
            icon: 'error',
            title: 'A ocurrido un error',
            text: 'Something went wrong!',
          });
        }
      }else{
        swal.fire({
          icon: 'error',
          title: 'A ocurrido un error',
          text: 'Something went wrong!',
        });
      }
    });
  }

}
