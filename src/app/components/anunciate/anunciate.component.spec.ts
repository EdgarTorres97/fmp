import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnunciateComponent } from './anunciate.component';

describe('AnunciateComponent', () => {
  let component: AnunciateComponent;
  let fixture: ComponentFixture<AnunciateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AnunciateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnunciateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
