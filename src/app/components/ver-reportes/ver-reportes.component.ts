import { Component, OnInit } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import{Router,ActivatedRoute} from '@angular/router';
import{AuthService}from '../../services/auth.service';
import { LostPetReport } from 'src/app/models/lostPetReport';
import { Municipality } from 'src/app/models/municipality';
import { PetType } from 'src/app/models/petType';
import { PetChip } from 'src/app/models/petChip';
import { PetTag } from 'src/app/models/petTag';
import { PetSex } from 'src/app/models/petSex';
import { LostPetReportViewModel } from 'src/app/models/lostPetReportViewModel';
import { User } from 'src/app/models/user';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-ver-reportes',
  templateUrl: './ver-reportes.component.html',
  styleUrls: ['./ver-reportes.component.css']
})
export class VerReportesComponent implements OnInit {
  httpOptions = {
    headers: new HttpHeaders()
  };
  public titulo: string;
  public token: string;
  public municipios: Municipality[];
  public petTypes: PetType[];
  public petChips: PetChip[];
  public petTags: PetTag[];
  public petSexes: PetSex[];
  public lostPetReport: LostPetReport;
  public lostPetReports: LostPetReportViewModel[];
  public api: string;
  public error: string;
  public errorBol: boolean;
  public user: User;
  public configurar: boolean;
  public id_process: number;
  public rutaReporte: string;
  constructor(
    public httpVar:HttpClient,
    private router: Router,
    private authService: AuthService,
    private spinner:NgxSpinnerService
  ) {
    this.titulo = 'Mascotas perdidas y encontradas';
    this.httpOptions.headers = new HttpHeaders({
      'Authorization': `Bearer ${this.authService.token}`
    });
    this.api = this.authService.api;
    this.municipios = [];
    this.petTypes = [];
    this.petChips = [];
    this.petTags = [];
    this.petSexes = [];
    this.lostPetReports = [];
    this.error = '';
    this.errorBol = false;
    this.inicializarLostPetReport();
    this.configurar = false;
    this.user = authService.retornarDataUser();
    this.id_process = 2;
    this.rutaReporte = 'reportes';
   }

  ngOnInit(): void {
    if(this.user.id_role == 1){
      this.configurar = true;

    }else{
      this.configurar = false;
    }
    
    
    this.obtenerListaMunicipios();
    this.obtenerListaPetTypes();
    this.obtenerListaPetChips();
    this.obtenerListaPetTags();
    this.obtenerListaPetSexs();
    //this.spinner.show();
    this.obtenerListaReportes();
    
  }
  inicializarLostPetReport(){
    this.lostPetReport = {
      id:null,
      id_user:null,
      id_petType: null,
      petType: {
        id:null,
        ptpCounter:null,
        ptpName: null
      },
      id_petSex: null,
      petSex: {
        id:null,
        psCounter:null,
        psName: null
      },
      id_petChip: null,
      petChip: {
        id: null,
        pcCounter: null,
        pcName: null
      },
      id_petTag: null,
      petTag:{
        id: null,
        ptCounter: null,
        ptName: null
      },
      id_petProcess:null,
      id_petLocation: null,
      petLocation:{
        id:null,
        plLostDate: null,
        plLote: null,
        plManzana: null,
        plPostalCode: null,
        plRegion: null,
        plLatitude: null,
        plLongitude: null,
        id_municipality:null,
        plMunicipality: {
          id: null,
          mnCounter: null,
          mnName: null
        }

      },
      lprBreedName: null,
      lprSize: null,
      lprAge: null,
      lprPetName:null,
      lprPhoneNumber:null,
      lprTail: null,
      lprEar: null,
      lprColor: null,
      lprObservation: null,
      }
  }
  obtenerListaMunicipios(){
    this.httpVar.get<Municipality>(this.api+'municipios',this.httpOptions)
    .subscribe(data=>{
      //console.log(data[1]);
      //console.log(data);
      for (let key in data) {
        //console.log(value);
        let customObj = 
        new Municipality(
          data[key]['id'],
          data[key]['mnName'],
          null
          );
        this.municipios.push(customObj);
      }
    });
  }
  obtenerListaPetTypes(){
    this.httpVar.get<PetType>(this.api+'petTypes',this.httpOptions)
    .subscribe(data=>{
      //console.log(data[1]);
      //console.log(data);
      for (let key in data) {
        //console.log(value);
        let customObj = 
        new PetType(
          data[key]['id'],
          data[key]['ptpName'],
          null
          );
        this.petTypes.push(customObj);
      }
    });
  }
  obtenerListaPetChips(){
    this.httpVar.get<PetChip>(this.api+'petChips',this.httpOptions)
    .subscribe(data=>{
      //console.log(data[1]);
      //console.log(data);
      for (let key in data) {
        //console.log(value);
        let customObj = 
        new PetChip(
          data[key]['id'],
          data[key]['pcName'],
          null
          );
        this.petChips.push(customObj);
      }
    });
  }
  obtenerListaPetTags(){
    this.httpVar.get<PetTag>(this.api+'petTags',this.httpOptions)
    .subscribe(data=>{
      //console.log(data[1]);
      //console.log(data);
      for (let key in data) {
        //console.log(value);
        let customObj = 
        new PetTag(
          data[key]['id'],
          data[key]['ptName'],
          null
          );
        this.petTags.push(customObj);
      }
    });
  }
  obtenerListaPetSexs(){
    this.httpVar.get<PetSex>(this.api+'petSexes',this.httpOptions)
    .subscribe(data=>{
      //console.log(data[1]);
      //console.log(data);
      for (let key in data) {
        //console.log(value);
        let customObj = 
        new PetSex(
          data[key]['id'],
          data[key]['psName'],
          null
          );
        this.petSexes.push(customObj);
      }
    });
  }
  obtenerListaReportes(){ 
    // console.log(this.lostPetReport);
    if(this.user.id_role==1){
      this.rutaReporte = 'reportesConfiguracion'
    }
    this.lostPetReports=[];
    this.httpVar.post<LostPetReport>(this.api+this.rutaReporte,this.lostPetReport,this.httpOptions)
    .subscribe(data=>{
      // console.log(data);
      if(data){
        if(!data['error']){
          this.errorBol = false;
          for (let key in data) {
            let value = data[key]['id'];
            //console.log(value);
            let customObj = new LostPetReportViewModel(
              data[key]['id'],
              data[key]['id_user'],
              data[key]['id_petType'],
              data[key]['id_petSex'],
              data[key]['id_petChip'],
              data[key]['id_petTag'],
              data[key]['id_petProcess'],
              data[key]['ppName'],
              data[key]['id_petLocation'],
              data[key]['lprBreedName'],
              data[key]['lprSize'],
              data[key]['lprAge'],
              data[key]['lprTail'],
              data[key]['lprEar'],
              data[key]['lprColor'],
              data[key]['lprObservation'],
              data[key]['lprPhoneNumber'],
              data[key]['lprPetName'],
              data[key]['plLostDate'],
              data[key]['plLote'],
              data[key]['plManzana'],
              data[key]['plPostalCode'],
              data[key]['plRegion'],
              data[key]['pllatitude'],
              data[key]['pllongitude'],
              data[key]['mnName'],
              data[key]['pcName'],
              data[key]['piImageUrl'],
              data[key]['psName'],
              data[key]['ptName'],
              data[key]['ptpName'],
            );
            
            this.lostPetReports.push(customObj);
            
          }
          //this.spinner.hide();
        }else{
          //this.spinner.hide();
          this.errorBol = true;
          this.error = data['error'];
        }
      }
      else{
        //this.spinner.hide();
        this.errorBol = true;
        this.error = 'A ocurrido un error';
      }
    });
  }
}
