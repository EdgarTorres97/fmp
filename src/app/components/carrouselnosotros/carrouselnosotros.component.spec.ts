import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarrouselnosotrosComponent } from './carrouselnosotros.component';

describe('CarrouselnosotrosComponent', () => {
  let component: CarrouselnosotrosComponent;
  let fixture: ComponentFixture<CarrouselnosotrosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CarrouselnosotrosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CarrouselnosotrosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
