import { Municipality } from 'src/app/models/municipality';
import { PetType } from 'src/app/models/petType';
import { PetChip } from 'src/app/models/petChip';
import { PetTag } from 'src/app/models/petTag';
import { PetSex } from 'src/app/models/petSex';
import { PetLocation } from './petLocation';
export class LostPetReport{
    constructor(
    public id:number,
    public id_user:number,
    public id_petType: number,
    public petType: PetType,
    public id_petSex: number,
    public petSex: PetSex,
    public id_petChip: number,
    public petChip: PetChip,
    public id_petTag: number,
    public petTag: PetTag,
    public id_petProcess: number,
    public id_petLocation: number,
    public petLocation: PetLocation,
    public lprBreedName: string,
    public lprSize: string,
    public lprAge: string,
    public lprTail: string,
    public lprPetName: string,
    public lprPhoneNumber: string,
    public lprEar: string,
    public lprColor: string,
    public lprObservation: string,
    ){
        
    }
}
