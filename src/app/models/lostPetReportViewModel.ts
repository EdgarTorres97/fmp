import { Municipality } from 'src/app/models/municipality';
import { PetType } from 'src/app/models/petType';
import { PetChip } from 'src/app/models/petChip';
import { PetTag } from 'src/app/models/petTag';
import { PetSex } from 'src/app/models/petSex';
import { PetLocation } from './petLocation';
export class LostPetReportViewModel{
    constructor(
    public id:number,
    public id_user:number,
    public id_petType: number,
    public id_petSex: number,
    public id_petChip: number,
    public id_petTag: number,
    public id_petProcess: number,
    public ppName: string,
    public id_petLocation: number,
    public lprBreedName: string,
    public lprSize: string,
    public lprAge: string,
    public lprTail: string,
    public lprEar: string,
    public lprColor: string,
    public lprObservation: string,
    public lprPhoneNumber: string,
    public lprPetName: string,
    public plLostDate: Date,
    public plLote: number,
    public plManzana: string,
    public plPostalCode: string,
    public plRegion: number,
    public pllatitude: string,
    public pllongitude: string,
    public mnName: string,
    public pcName: string,
    public piImageUrl: string,
    public psName: string,
    public ptName: string,
    public ptpName: string,
    ){
        
    }
}
