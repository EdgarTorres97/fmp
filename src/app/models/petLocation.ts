import { Municipality } from './municipality';

export class PetLocation{
    constructor(
    public id:number,
    public id_municipality:number,
    public plMunicipality: Municipality,
    
    public plManzana: number,
    public plRegion: number,
    public plLote: number,
    public plPostalCode: number,
    public plLostDate: Date,
    public plLatitude: string,
    public plLongitude: string,
    ){
        
    }
}