export class UserViewModel{
    constructor(
    public id:number,
    public name: string,
    public email: string,
    public password: string,
    public phoneNumber: string,
    public age: number,
    public id_role: number,
    public roleName: string,
    public description: string,
    public urlImage: string
    ){
        
    }
}