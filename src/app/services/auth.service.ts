import { Injectable } from '@angular/core';
import{User}from '../models/user';
import * as CryptoJS from 'crypto-js';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public estado: boolean;
  public token: string;
  public correo: string;
  public usuario: User;
  public api: string;
  public encPass: string;
  public userDecrypt: User;
  public validaToken: boolean;
  httpOptions = {
    headers: new HttpHeaders()
  };
  constructor(
    public httpVar:HttpClient,
  ) { 
    this.usuario = 
    {
      id: 0,
      name:'',
      email: '',
      phoneNumber:'',
      password: '',
      id_role: 4,
      age:0,
      urlImage:'',
      token:''
    }
    // this.api = 'https://fmpback.ryumicon.com/public/api/';
    this.api = 'http://localhost:8002/api/';
    this.encPass = 'password';
    this.userDecrypt = {
      id:null,
      name:null,
      email:null,
      phoneNumber:null,
      age: null,
      id_role: null,
      urlImage:null,
      password:null,
      token:null
    };
    this.validaToken = false;
    this.retornarDataUser();
    this.token = this.userDecrypt.token;
  }
  
  obtenerToken(){
    if(this.userDecrypt.token){
      this.estado = true;
    }else{
      this.estado = false;
      //console.log("No hay token");
    }
    return this.estado;

  }
  retornarToken(){
    try{
      // console.log(this.usuario);
      if(JSON.parse(localStorage.getItem('usuario'))){
        this.usuario = this.retornarDataUser();
      //console.log(token);
      }else{
        return 'false';
      }
      return this.token;
    }catch (error) {
        console.log(error);
      }
  }
  retornarDataUser(){
    try{
      if(JSON.parse(localStorage.getItem('usuario'))){
        this.usuario = JSON.parse(localStorage.getItem('usuario'));
        this.userDecrypt = {
          id: this.desencriptar(this.usuario.id.toString()),
          name:this.desencriptar(this.usuario.name),
          email:this.desencriptar(this.usuario.email),
          phoneNumber:this.desencriptar(this.usuario.phoneNumber),
          age: this.desencriptar(this.usuario.age.toString()),
          id_role: this.desencriptar(this.usuario.id_role.toString()),
          urlImage:this.desencriptar(this.usuario.urlImage),
          password: '',
          token:this.desencriptar(this.usuario.token)
        };

        // console.log(this.usuario);
      }else{
        this.userDecrypt.id_role = 4;
      }
    }catch (error) {
     console.log(error);
    }
    
      return this.userDecrypt;
  }
  encriptar(texto: string){
      return CryptoJS.AES.encrypt(texto.trim(),this.encPass.trim()).toString();
  }
  desencriptar(texto:string){
    return CryptoJS.AES.decrypt(texto.trim(),this.encPass.trim()).toString(CryptoJS.enc.Utf8);
  }
}
