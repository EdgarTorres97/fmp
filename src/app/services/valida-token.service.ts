import { Injectable } from '@angular/core';
import{User}from '../models/user';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import{AuthService}from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class ValidaTokenService {
  public token: string;
  public usuario: User;
  public userDecrypt: User;
  public validaToken: boolean;
  httpOptions = {
    headers: new HttpHeaders()
  };
  constructor(
    public httpVar:HttpClient,
    private authService: AuthService,
  ) {
    this.usuario = 
    {
      id: 0,
      name:'',
      email: '',
      phoneNumber:'',
      password: '',
      id_role: 4,
      age:0,
      urlImage:'',
      token:''
    }
    this.userDecrypt = {
      id:null,
      name:null,
      email:null,
      phoneNumber:null,
      age: null,
      id_role: null,
      urlImage:null,
      password:null,
      token:null
    };
   }
   validaTokenURL(){
    try{
      console.log('Entro');
      if(this.authService.userDecrypt.id_role != 4){
        
        this.httpOptions.headers = new HttpHeaders({
          'Authorization': `Bearer ${this.authService.token}`
        }); 
      this.httpVar.get<boolean>(this.authService.api+'validarToken',this.httpOptions)
      .subscribe(data=>{
        console.log(data);
        this.validaToken = data;
        if(this.validaToken){
          return true;
        }
          return false;
      });
      }
      
      return false;
    }catch (error) {
        console.log(error);
        return false;
      }
  }
}
