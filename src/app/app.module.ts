import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {routing,appRoutingProviders} from './app.routing';
import{FormsModule, ReactiveFormsModule,FormBuilder,
  FormGroup,
  FormArray,
  FormControl,
  ValidatorFn} from '@angular/forms';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { MascotasComponent } from './components/mascotas/mascotas.component';
import { AnunciateComponent } from './components/anunciate/anunciate.component';
import { RankingComponent } from './components/ranking/ranking.component';
import { NosotrosComponent } from './components/nosotros/nosotros.component';
import { AccederComponent } from './components/acceder/acceder.component';
import { RegistroComponent } from './components/registro/registro.component';
import { ErrorComponent } from './components/error/error.component';
import { GoogleMapsModule } from '@angular/google-maps';
import { CarrouselComponent } from './components/carrousel/carrousel.component'
import { HttpClientModule } from '@angular/common/http';
import { PerfilComponent } from './components/perfil/perfil.component';
import { CrearReporteComponent } from './components/crear-reporte/crear-reporte.component';
import swal from'sweetalert2';
import { VerReportesComponent } from './components/ver-reportes/ver-reportes.component';
import { ModificarPerfilComponent } from './components/modificar-perfil/modificar-perfil.component';
import { MisReportesComponent } from './components/mis-reportes/mis-reportes.component';
import { ReporteComponent } from './components/reporte/reporte.component';
import { PanelComponent } from './components/panel/panel.component';
import { ConfigurarReporteComponent } from './components/configurar-reporte/configurar-reporte.component';
import { UsersComponent } from './components/users/users.component';
import { DataTablesModule } from 'angular-datatables';
import { UsersConfigurationComponent } from './components/users-configuration/users-configuration.component';
import { UsersDetailComponent } from './components/users-detail/users-detail.component';
import { InfoReporteComponent } from './components/info-reporte/info-reporte.component';
import { CarrouselnosotrosComponent } from './components/carrouselnosotros/carrouselnosotros.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UsersCreateComponent } from './components/users-create/users-create.component';
import { IniciocardsComponent } from './components/iniciocards/iniciocards.component';
import { UsersTableComponent } from './components/users-table/users-table.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    InicioComponent,
    MascotasComponent,
    AnunciateComponent,
    RankingComponent,
    NosotrosComponent,
    AccederComponent,
    RegistroComponent,
    ErrorComponent,
    CarrouselComponent,
    PerfilComponent,
    CrearReporteComponent,
    VerReportesComponent,
    ModificarPerfilComponent,
    MisReportesComponent,
    ReporteComponent,
    PanelComponent,
    ConfigurarReporteComponent,
    UsersComponent,
    UsersConfigurationComponent,
    UsersDetailComponent,
    InfoReporteComponent,
    CarrouselnosotrosComponent,
    UsersCreateComponent,
    IniciocardsComponent,
    UsersTableComponent,
  ],
  imports: [
    BrowserModule,
    routing,
    GoogleMapsModule,
    FormsModule,                               // <========== Add this line!
    ReactiveFormsModule,
    HttpClientModule, 
    DataTablesModule,
    NgxSpinnerModule,
    BrowserAnimationsModule
  ],
  providers: [appRoutingProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
