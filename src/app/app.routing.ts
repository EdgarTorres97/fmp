import{ModuleWithProviders} from '@angular/core';
import{Routes,RouterModule} from '@angular/router';
import { AccederComponent } from './components/acceder/acceder.component';
import { AnunciateComponent } from './components/anunciate/anunciate.component';
import { ConfigurarReporteComponent } from './components/configurar-reporte/configurar-reporte.component';
import { CrearReporteComponent } from './components/crear-reporte/crear-reporte.component';
import { ErrorComponent } from './components/error/error.component';
import { InfoReporteComponent } from './components/info-reporte/info-reporte.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { MascotasComponent } from './components/mascotas/mascotas.component';
import { MisReportesComponent } from './components/mis-reportes/mis-reportes.component';
import { ModificarPerfilComponent } from './components/modificar-perfil/modificar-perfil.component';
import { NosotrosComponent } from './components/nosotros/nosotros.component';
import { PanelComponent } from './components/panel/panel.component';
import { PerfilComponent } from './components/perfil/perfil.component';
import { RankingComponent } from './components/ranking/ranking.component';
import { RegistroComponent } from './components/registro/registro.component';
import { UsersConfigurationComponent } from './components/users-configuration/users-configuration.component';
import { UsersDetailComponent } from './components/users-detail/users-detail.component';
import { UsersComponent } from './components/users/users.component';
import { VerReportesComponent } from './components/ver-reportes/ver-reportes.component';
import { AdminGuard } from './guards/admin.guard';
import { AuthGuard } from './guards/auth.guard';
import { NuevoGuard } from './guards/nuevo.guard';


//componentes que son una sección de la aplicación web.


const appRoutes: Routes = [
    {path:'',component:InicioComponent},
    {path:'inicio',component:InicioComponent},
    //{path:'mascotas',component:MascotasComponent},
    {path:'anunciate',component:AnunciateComponent,canActivate:[AuthGuard]},
    {path:'ranking',component:RankingComponent},
    {path:'nosotros',component:NosotrosComponent},
    {path:'acceder',component:AccederComponent,canActivate:[NuevoGuard]},
    {path:'registro',component:RegistroComponent,canActivate:[NuevoGuard]},
    {path:'perfil',component:PerfilComponent,canActivate:[AuthGuard]},
    {path:'perfil/modificar',component:ModificarPerfilComponent,canActivate:[AuthGuard]},
    {path:'crearReporte',component:CrearReporteComponent,canActivate:[AuthGuard]},
    {path:'reportes',component:VerReportesComponent},
    {path:'perfil/mis-reportes',component:MisReportesComponent,canActivate:[AuthGuard]},
    {path:'panel',component:PanelComponent,canActivate:[AdminGuard]},
    {path:'usuarios',component:UsersComponent,canActivate:[AdminGuard]},
    {path:'usuarios/configuracion/:id',component:UsersConfigurationComponent,canActivate:[AdminGuard]},
    {path:'usuarios/detalle/:id',component:UsersDetailComponent,canActivate:[AdminGuard]},
    {path:'reportes/:id',component:ConfigurarReporteComponent,canActivate:[AdminGuard]},
    {path:'reportes/info/:id',component:InfoReporteComponent},

    {path: '**', component: ErrorComponent}
];

export const appRoutingProviders: any[] = [];
export const routing:ModuleWithProviders<any> = RouterModule.forRoot(appRoutes);
